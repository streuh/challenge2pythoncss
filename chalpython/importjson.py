import json

# BONUS Function which returns the number of colors contained in the json file.
# @param includeChildren : boolean
# return integer; number of colors
def get_total(filename, include_children):
    json_file = open(filename)
    unfiltered_json = json.load(json_file)

    if include_children == True :

        array_children_number = sum([len(i['children']) for i in unfiltered_json]) + len(unfiltered_json)

        return(array_children_number)

    else :

        total = len(unfiltered_json)
    
    json_file.close()

    return(total)

# Function which reads a JSON file
# And returns a structured array.
# @param string filename
# return array
def json_to_array(filename):
        #Opening the json file as an array
        json_data=open(filename)
        colors = json.load(json_data)

        #Creating the empty list result to use later on
        result=[]
 
        #For (each) loop to get every value needed in the result array
        for value in colors:
                parent=[value["name"], value["hex"], []]
                children=value["children"]

                for j in range(0,len(children)):
                        child=[children[j]["name"], children[j]["hex"], children[j]["company"]]
                        parent[2].append(child)

                result.append(parent)

        json_data.close()
        return(result)

#print(json_to_array("../json/colors.json")) #To be able to see the result of the function

#@param array: first function
def array_to_html(array):

	#true_array is used to have access to the list returned from the function
    true_array = array('json/colors.json')

    #Beginning of the html code
    code_html = '<!doctype html>\n<html>\n<head>\n	<meta charset="utf-8">\n	<title>Python_html</title>\n</head>\n<body>\n	<table>\n	' 

    #For (each) loop to write the name/hex of the parent in the table
    for color in true_array:
            line='	<tr>\n			<td style="background-color:'+color[1] +'">\n				<p>'+color[0] +'</p>\n				<p> '+ color[1] + '</p>\n			</td>\n'

            #For (each) loop to write the name/hex of the children in the table
            for children in color[2]:
            	line = line + '			<td style="background-color:'+ children[1] +'">\n				<p>'+ children[0] +'</p>\n				<p> '+ children[1] + '</p>\n				<p>' +	children[2] + '</p>\n			</td>\n'
            code_html = code_html + line + '	</tr>\n'

    code_html_end='\n	</table>\n</body>\n</html>'
    code_html= code_html + code_html_end
    return (code_html)

#print(array_to_html(json_to_array)) #To be able to see the result of the function





# Save an HTML string to a given file.
#@param filename : path to output file
#@param array_to_html_function :  second function
def html_to_file(filename, array_to_html_function):

	#To create your html file
    my_html_file = open(filename, "w+") #créer le fichier si il n'existe pas  

    #function_result is used to have access to the string returned from the function
    function_result = array_to_html_function(json_to_array)

    my_html_file.write(function_result)

    my_html_file.close()



html_to_file("html/nuancier.html", array_to_html)


# BONUS function which returns the children of a node.
# @param string reprsenting a color name
# return array
def get_children(node_name):
	
    array = json_to_array("colors.json")

    for value in array:

        if node_name == value[0]:

            return(value[2])

#print(get_children('Yellow')) #To be able to see the result of the function