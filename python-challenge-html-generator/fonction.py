import json
import os
import sys

def jsonToArray(filename):
	file = open(filename, "rt")   #Open File
	array = file.read()			  #read File

	file.close()                  # Close File 
	data = json.loads(array)      # decript File

	tab = []                     #création du tableau
	for colorName in data:       #boucle colorName dans le fichier décrypté 
		name = colorName['name'] #var qui recup name  
		hexa = colorName['hex']  #var qui recup hex 
		color = [name, hexa]     #var qui recup dans le tableau les 2 resultat   
		tab.append(color) 	     #recupere color, et place le dans tab
	
	return tab


def jsonToArray(filename):
    # define var = json file name
    filename = "colors.json"

    # open json file & read it, close it when done
    with open(filename) as json_colors:
        # stock file in colors_item
           colors_item = json.load(json_colors)
    # create array to insert values from for loop
    array = []
    # for loop to browse colors_item & append requested values
    for value in colors_item:
        array.append(value['name'])
        array.append(value['hex'])
    return array
    

def arrayToHtml(array):
	htmldebut = """<!DOCTYPE html> <html> <head title="Monsite"> <meta charset="utf-8"></head> <body>""" 
    divcolor = """<div style="background-color: """
    enddivcolor = """ ";><h1>"""
    middleColor =  """</h1></div>"""
    htmlfin = """</body> </html>""" #variable qui contient le html
    i = 0 # variable qui initialise la valeur i a 0  
    while i<len(colors): #tant que i est infèrieur a la longueur du tableau (colors) While est une boucle conditionnelle
        if i % 2 == 0 or i == 0: # si i est pair ou egale a 0
            intermediaire2 = enddivcolor + colors[i] + middleColor #alors tu concatene la var endivcolor avec le titre (h1) + la var middlecor 
        if i % 2 == 1 or i == 1: # si i est impaire ou egale a 1 
            intermediaire =  divcolor + colors[i] # alors concatene la variable divcolor avec l'exa (color[i])
            htmldebut = htmldebut + intermediaire + intermediaire2 
        i=i+1 #alors rajoute 1 a i

    HTML = htmldebut + htmlfin

    return HTML

def htmlToFile(HTML):
	html_file = open("index.html", "w")
	html_file = write(HTML)
	html_file.close

htmlToFile(arrayToHtml(jsonToArray(('colors.json')))
  
############################################################################################

def jsonToArray(filename):  
    filename = "colors.json"
  
    with open(filename) as json_colors:

           colors_item = json.load(json_colors)

    array = []
    
    for value in colors_item:
        array.append(value['name'])
        array.append(value['hex'])
    return array          


def arrayToHtml(colors):
	htmldebut = """<!DOCTYPE html> <html> <head title="Monsite"> <meta charset="utf-8"></head> <body>""" 
    divcolor = """<div style="background-color: """
    enddivcolor = """ ";><h1>"""
    middleColor =  """</h1></div>"""
    htmlfin = """</body> </html>"""
    i = 0
    while i<len(colors): 

        if i % 2 == 0 or i == 0:

            intermediaire2 = enddivcolor + colors[i] + middleColor 

        if i % 2 == 1 or i == 1:

            intermediaire =  divcolor + colors[i] 
			htmldebut = htmldebut + intermediaire + intermediaire2 
        i=i+1

    HTML = htmldebut + htmlfin

    return HTML

# Save an HTML string to a given file.
# @param filename : path to output file
# @param htmlString : content to save in file
# return absolute path to filename

def htmlToFile(HTML):

	html_file = open("index.html", "w")
	html_file = write(HTML)
	html_file.close()

htmlToFile(arrayToHtml(jsonToArray('colors.json')))

# BONUS function which returns the children of a node.
#@param string reprsenting a color name
#return array
def getChildren(nodeName):
	TODO
	return [];

	 
