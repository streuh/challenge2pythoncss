import html_manager
import unittest
import os.path

class HTMLTestManager(unittest.TestCase):

	def test_total_number(self):
		"""Open the JSON file and count the records"""
		count=html_manager.getTotal("colors.json", False)
		self.assertEqual(count, 11, "Invalid count of records in file.JSON")

	def test_jsonToArray(self):
		""" """
		result=html_manager.jsonToArray("colors.json")
		self.assertTrue(isinstance(result, list), "Result returned is not an array")

	def test_arrayToString(self):
		""""""
		result=html_manager.arrayToHtml([])
		self.assertTrue(isinstance(result, str), "You result is not a string!!!! ")
	def test_htmlToFile(self):
		absolutePath = html_manager.htmlToFile("./html_manager.py", '<div></div>')
		self.assertTrue(os.path.isfile(absolutePath), "Your file has not been generated !")

	def test_getChildren(self):
		children = html_manager.getChildren("Yellow")
		self.assertTrue(isinstance(children, list), "No array has been returned")

